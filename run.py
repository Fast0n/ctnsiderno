import json
import logging
import os
import re
import time
from pathlib import Path

from facebook_scraper import get_posts
from pid.decorator import pidfile
from telegram.ext import Updater

from settings import CHAT_ID, TOKEN, image_null, url_fb

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.ERROR)

logger = logging.getLogger(__name__)


def readJson(bot):
    with open(os.getcwd() + "/data.json", "r") as f:
        readData = json.load(f)
        for key, value in readData.items():
            bot.send_photo(chat_id=CHAT_ID, caption=remove_urls(
                readData[key]["0"]), photo=readData[key]["1"])


# Get post
def generate_json_from_html():
    i = 0
    jsonFeed = {}
    for post in get_posts(url_fb, pages=3, cookies="cookies.txt", timeout=60):

        if i < 10:
            time_post = post['time']
            message_post = post['text']
            image_post = post['image']

            if image_post != None:
                image_post = post['image']
            else:
                image_post = image_null

            if message_post is not None and "ore" in message_post.lower():
                jsonFeed[str(time_post)] = {}
                jsonFeed[str(time_post)]["0"] = message_post
                jsonFeed[str(time_post)]["1"] = image_post
                i += 1

        else:
            break
    return {i: jsonFeed[i] for i in reversed(jsonFeed)}


def job(bot):
    try:
        if not Path(os.getcwd() + "/data.json").is_file():

            obj = open(os.getcwd() + "/data.json", "w")
            obj.write(json.dumps(generate_json_from_html()))
            obj.close()
            readJson(bot)

        else:
            with open(os.getcwd() + "/data.json", "r") as f:
                readData = json.load(f)
                diff = generate_json_from_html().keys() - readData.keys()

            if diff:
                obj = open(os.getcwd() + "/data.json", "w")
                obj.write(json.dumps(generate_json_from_html()))
                obj.close()
                with open(os.getcwd() + "/data.json", "r") as f:
                    readData = json.load(f)
                    for key in diff:
                        bot.send_photo(chat_id=CHAT_ID, caption=remove_urls(
                            readData[key]["0"]), photo=readData[key]["1"])
    except:
        print('json non valido')


def remove_urls(vTEXT):
    vTEXT = re.sub(r"http\S+", "", vTEXT)
    return(vTEXT)


@ pidfile(pidname='/tmp/CTNSiderno.pid')
def main():
    print("--- Starting CTNSiderno ---")
    # Setup bot
    updater = Updater(TOKEN, use_context=True)
    dispatcher = updater.dispatcher

    while True:
        job(updater.bot)
        time.sleep(60 * 60 * 1)  # wait 2 hours


if __name__ == "__main__":
    main()
